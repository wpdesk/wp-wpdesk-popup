[![pipeline status](https://gitlab.com/wpdesk/wp-wpdesk-popup/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-wpdesk-popup/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-popup/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-wpdesk-popup/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-wpdesk-popup/v/stable)](https://packagist.org/packages/wpdesk/wp-wpdesk-popup) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-wpdesk-popup/downloads)](https://packagist.org/packages/wpdesk/wp-wpdesk-popup) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-wpdesk-popup/v/unstable)](https://packagist.org/packages/wpdesk/wp-wpdesk-popup) 
[![License](https://poser.pugx.org/wpdesk/wp-wpdesk-popup/license)](https://packagist.org/packages/wpdesk/wp-wpdesk-popup) 

# wp-wpdesk-popup

