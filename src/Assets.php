<?php
/**
 * Class Assets
 *
 * @package WpDesk\Popup
 */

namespace WpDesk\Popup;

/**
 * Assets
 */
class Assets {

    public function enqueue( $base_url, $suffix = '', $scripts_version = '' ) {
    	$suffix = '';
	    wp_register_style( 'wpdesk_popup_css', trailingslashit( $base_url ) . 'vendor_prefixed/wpdesk/wpdesk-popup/assets/css/popup' . $suffix . '.css', array(), $scripts_version );
	    wp_enqueue_style( 'wpdesk_popup_css' );

	    wp_enqueue_script( 'wpdesk_popup', trailingslashit( $base_url ) . 'vendor_prefixed/wpdesk/wpdesk-popup/assets/js/popup' . $suffix . '.js', array(), $scripts_version );
    }

}
